<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Evan Himawan Saragih</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <style>
        .container {
            background-color: grey;
        }

        .col {
            background-color: steelblue;
            padding: 20px;
            margin: 10px;
            text-align: center;
            color: white;
        }

        .today {
            color: green;
        }

        .day-selected {
            color: steelblue;
        }
        button{
        border: 0px solid #ffffff;
        }
        button:focus{
        border: 1px solid #ffffff;
        box-shadow: -1px 2px 41px -7px rgba(0, 0, 0, 0.33);
        }
    </style>
</head>

<body>
    <?php
    $day = date('l'); ?>
    <h3 style="text-align: center;">Today is <label class="today"><?= $day ?></label></h3> <br>
    <div class='container'>
        <div class='row'>
            <?php
            $days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
            foreach ($days as $data) {
                if ($data == $day) {
                    echo "<button id='klik' class='col' style='background-color:green' value='$day'>
                $data
            </button>";
                } else {
                    echo "<button id='klik' class='col' value='$data'>
                $data
            </button>";
                }
            }
            ?>
        </div>
    </div>
    <br>
    <h3 style="text-align: center;">Selected day is <label class="day-selected"><?= $day ?></label></h3>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript">
        $("button").click(function() {
            var select_day = $(this).val();
            $(".day-selected").text(select_day);
        });
    </script>
</body>

</html>